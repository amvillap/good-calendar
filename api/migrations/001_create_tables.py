steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            first_name VARCHAR NOT NULL,
            last_name VARCHAR NOT NULL,
            username VARCHAR NOT NULL,
            password VARCHAR NOT NULL,
            email VARCHAR NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """
    ],
    [
        """
        CREATE TABLE task_categories (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR NOT NULL,
            description TEXT
        );
        """,
        """
        DROP TABLE task_categories;
        """
    ],
    [
        """
        CREATE TABLE event_categories (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR NOT NULL
        );
        """,
        """
        DROP TABLE event_categories;
        """
    ],
    [
        """
        CREATE TABLE events (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR NOT NULL,
            description TEXT,
            date DATE NOT NULL,
            event_category_id INT REFERENCES event_categories(id)
        );
        """,
        """
        DROP TABLE events;
        """
    ],
    [
        """
        CREATE TABLE tasks (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR NOT NULL,
            description TEXT,
            date_created DATE NOT NULL DEFAULT CURRENT_DATE,
            date_due DATE,
            time_due TIME,
            notes TEXT,
            link VARCHAR,
            task_category_id INT REFERENCES task_categories(id),
            event_id INT REFERENCES events(id),
            completed BOOLEAN DEFAULT false
        );
        """,
        """
        DROP TABLE tasks;
        """
    ],
    [
        """
        CREATE TABLE subtasks (
            id SERIAL PRIMARY KEY NOT NULL,
            task_id INT REFERENCES tasks(id),
            title VARCHAR NOT NULL,
            date_created DATE NOT NULL DEFAULT CURRENT_DATE,
            date_due DATE,
            time_due TIME,
            completed BOOLEAN DEFAULT false
        );
        """,
        """
        DROP TABLE subtasks;
        """
    ]
]
