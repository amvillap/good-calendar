from pydantic import BaseModel
from queries.pool import pool


class UserIn(BaseModel):
    first_name: str
    last_name: str
    username: str
    email: str
    password: str


class UserOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    username: str
    email: str


class UserOutWithPassword(UserOut):
    hashed_password: str


class UserRepo:
    def record_to_user_out(self, record):
        user_dict = {
            "id": record[0],
            "first_name": record[1],
            "last_name": record[2],
            "username": record[3],
            "email": record[4],
            "password": record[5]
        }
        return user_dict

    def create_user(self, user: UserIn, hashed_password: str) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO users
                        (first_name,
                        last_name,
                        username,
                        email,
                        password)
                    VALUES
                        (%s, %s, %s, %s, %s)
                    RETURNING
                    id,
                    first_name,
                    last_name,
                    username,
                    email,
                    password;
                    """,
                    [
                        user.first_name,
                        user.last_name,
                        user.username,
                        user.email,
                        hashed_password,
                    ]
                )
                id = result.fetchone()[0]
                return UserOut(
                    id=id,
                    first_name=user.first_name,
                    last_name=user.last_name,
                    username=user.username,
                    email=user.email,
                )
