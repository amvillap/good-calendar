from fastapi import (
    APIRouter,
    Depends,
)
from pydantic import BaseModel
from queries.users import (
    UserIn,
    UserOut,
    UserRepo,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

router = APIRouter()


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: UserOut


@router.post("/api/users", response_model=AccountToken)
async def create_user(
    user: UserIn,
    repo: UserRepo = Depends()
):
    hashed_password = authenticator.hash_password(user.password)
    account = repo.create_user(user, hashed_password)
    form = AccountForm(username=user.username, password=user.password)
    token = await authenticator.login(form, repo)
    return AccountToken(account=account, **token.dict())
